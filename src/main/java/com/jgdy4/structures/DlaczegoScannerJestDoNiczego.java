package com.jgdy4.structures;

import java.util.Scanner;

public class DlaczegoScannerJestDoNiczego {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        String l1 = scanner.nextLine();
        System.out.println("Linia 1: " + l1);

        String l2 = scanner.next();
        System.out.println("Linia 2: " + l2);

        String l3 = scanner.nextLine();
        System.out.println("Linia 3: " + l3);

        String l4 = scanner.next();
        System.out.println("Linia 4: " + l4);

        String l5 = scanner.nextLine();
        System.out.println("Linia 5: " + l5);
    }
}
