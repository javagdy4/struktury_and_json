package com.jgdy4.structures.zad1;

import java.util.*;

public class MainComparable {
    public static void main(String[] args) {

        List<Osoba> list = new ArrayList<>(Arrays.asList(
                new Osoba("A", "B", 20),
                new Osoba("A1", "B7", 22),
                new Osoba("A2", "B6", 28),
                new Osoba("A3", "B5", 18),
                new Osoba("A4", "B4", 75),
                new Osoba("A5", "B3", 31),
                new Osoba("A6", "B2", 13),
                new Osoba("A7", "B1", 10),
                new Osoba("A8", "B8", 21)
        ));

        list.forEach(System.out::println); // wypisanie jedno pod drugim
        System.out.println(); // jedna linia przerwy

        Collections.sort(list); // w przypadku comparable nie jest wymagany comparator

        list.forEach(System.out::println); // wypisanie jedno pod drugim
        System.out.println(); // jedna linia przerwy


    }
}
