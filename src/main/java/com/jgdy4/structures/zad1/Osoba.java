package com.jgdy4.structures.zad1;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class Osoba implements Comparable<Osoba>{
    private String imie;
    private String nazwisko;
    private int wiek;

    @Override
    public int compareTo(Osoba osoba) {
//        int result = Integer.compare(wiek, osoba.getWiek());
//        if(result != 0){
//            return result;
//        }
//        return imie.compareTo(osoba.getImie());
        return Integer.compare(wiek, osoba.getWiek());
    }
}
