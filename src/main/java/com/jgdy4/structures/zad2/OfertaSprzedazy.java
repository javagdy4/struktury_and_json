package com.jgdy4.structures.zad2;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class OfertaSprzedazy {
    private String nazwa;
    private Double cena;

}
